<?php
/**
 *	Javo Themes functions and definitions
 *
 * @package WordPress
 * @subpackage Javo
 * @since Javo Themes 1.0
 */

 // Path Initialize
define( 'JVFRM_SPOT_APP_PATH'		, get_template_directory() );				// Get Theme Folder URL : hosting absolute path
define( 'JVFRM_SPOT_THEME_DIR'		, get_template_directory_uri() );			// Get http URL : ex) http://www.abc.com/
define( 'JVFRM_SPOT_SYS_DIR'		, JVFRM_SPOT_APP_PATH."/library");			// Get Library path
define( 'JVFRM_SPOT_TP_DIR'			, JVFRM_SPOT_APP_PATH."/templates");		// Get Tempate folder
define( 'JVFRM_SPOT_ADM_DIR'		, JVFRM_SPOT_SYS_DIR."/admin");				// Administrator Page
define( 'JVFRM_SPOT_IMG_DIR'		, JVFRM_SPOT_THEME_DIR."/assets/images");	// Images folder
define( 'JVFRM_SPOT_WG_DIR'			, JVFRM_SPOT_SYS_DIR."/widgets");			// Widgets Folder
define( 'JVFRM_SPOT_HDR_DIR'		, JVFRM_SPOT_SYS_DIR."/header");			// Get Headers
define( 'JVFRM_SPOT_CLS_DIR'		, JVFRM_SPOT_SYS_DIR."/classes");			// Classes
define( 'JVFRM_SPOT_DSB_DIR'		, JVFRM_SPOT_SYS_DIR."/dashboard");			// Dash Board
define( 'JVFRM_SPOT_FUC_DIR'		, JVFRM_SPOT_SYS_DIR."/functions");			// Functions
define( 'JVFRM_SPOT_PLG_DIR'		, JVFRM_SPOT_SYS_DIR."/plugins");			// Plugin folder
define( 'JVFRM_SPOT_ADO_DIR'		, JVFRM_SPOT_SYS_DIR . "/addons");			// Addons folder

define( 'JVFRM_SPOT_CUSTOM_HEADER', false );

// Includes : Basic or default functions and included files
require_once JVFRM_SPOT_SYS_DIR	. "/define.php";								// defines
require_once JVFRM_SPOT_SYS_DIR	. "/load.php";									// loading functions, classes, shotcode, widgets
require_once JVFRM_SPOT_SYS_DIR	. "/enqueue.php";								// enqueue js, css
require_once JVFRM_SPOT_SYS_DIR	. "/wp_init.php";								// post-types, taxonomies
require_once JVFRM_SPOT_ADM_DIR	. "/class-admin-theme-settings.php";			// theme options
require_once JVFRM_SPOT_DSB_DIR	. "/class-dashboard.php";						// theme screen options tab.

$jvfrm_spot_core_include	= apply_filters( 'jvfrm_spot_core_function_path', JVFRM_SPOT_APP_PATH .'/includes/class-core.php' );

if( file_exists( $jvfrm_spot_core_include ) ) require_once $jvfrm_spot_core_include;

do_action( 'jvfrm_spot_themes_loaded' );





// register custom post type to work with
function jvfrm_spot_create_post_type() {

    register_taxonomy(
        'portfolio_category',
        'portfolio',
        array(
            'labels' => array(
                'name' => 'Portofolio Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );

     register_taxonomy(
        'portfolio_tag',
        'portfolio',
        array(
            'labels' => array(
                'name' => 'Portofolio Tag',
                'add_new_item' => 'Add New Tag',
                'new_item_name' => "New Tag"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );

    register_taxonomy(
        'testimonial_category',
        'testimonial',
        array(
            'labels' => array(
                'name' => 'Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );


	$labels = array(
 		'name' => 'Portfolio',
    	'singular_name' => 'Portfolio',
    	'add_new' => 'Add New Portfolio',
    	'add_new_item' => 'Add New Portfolio',
    	'edit_item' => 'Edit Portfolio',
    	'new_item' => 'New Portfolio',
    	'all_items' => 'All Portfolio',
    	'view_item' => 'View Portfolio',
    	'search_items' => 'Search Portfolio',
    	'not_found' =>  'No Portfolio Found',
    	'not_found_in_trash' => 'No Portfolio found in Trash', 
    	'parent_item_colon' => '',
    	'menu_name' => 'Portfolio',
    );
	register_post_type( 'portfolio', array(
		'labels' => $labels,
		'has_archive' => true,
 		'public' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
		'taxonomies' => array( 'portfolio_category', 'portfolio_tag' ),	
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => 'portfolios' ),
		)
	);

	$labels = array(
 		'name' => 'Testimonial',
    	'singular_name' => 'Testimonial',
    	'add_new' => 'Add New Testimonial',
    	'add_new_item' => 'Add New Testimonial',
    	'edit_item' => 'Edit Testimonial',
    	'new_item' => 'New Testimonial',
    	'all_items' => 'All Testimonial',
    	'view_item' => 'View Testimonial',
    	'search_items' => 'Search Testimonial',
    	'not_found' =>  'No Testimonial Found',
    	'not_found_in_trash' => 'No Testimonial found in Trash', 
    	'parent_item_colon' => '',
    	'menu_name' => 'Testimonial',
    );
	register_post_type( 'testimonial', array(
		'labels' => $labels,
		'has_archive' => true,
 		'public' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
		'taxonomies' => array( 'testimonial_category' ),	
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => 'testimonial' ),
		)
	);
}
add_action( 'init', 'jvfrm_spot_create_post_type' );


if ( !class_exists('jvfrmPostCustomFields') ) {
 
    class jvfrmPostCustomFields {
        /**
        * @var  string  $prefix  The prefix for storing custom fields in the postmeta table
        */
        var $prefix = '_jvfrm_';
        /**
        * @var  array  $postTypes  An array of public custom post types, plus the standard "post" and "page" - add the custom types you want to include here
        */
        var $postTypes = array( "portfolio");
        /**
        * @var  array  $customFields  Defines the custom fields available
        */
        var $customFields = array(
            array(
                "name"          => "short-description",
                "title"         => "Short Description",
                "description"   => "",
                "type"          => "textarea",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_pages"
            ),
            array(
                "name"          => "creation-date",
                "title"         => "Date",
                "description"   => "Created date. ex) 01/01/2016 or 2016-01-01",
                "type"          =>   "text",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_posts"
            ),
            array(
                "name"          => "link-to",
                "title"         => "Link",
                "description"   => "Website link without 'http://' (ex: www.google.com)",
                "type"          =>   "text",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_posts"
            ),
            array(
                "name"          => "featured_portfolio",
                "title"         => "Featured Portfolio",
                "description"   => "Check if this portfolio is a featured one",
                "type"          => "checkbox",
                "scope"         =>   array( "portfolio" ),
                //"scope"         =>   array( "portfolio", "page" ),
                "capability"    => "manage_options"
            ),

			 array(
                "name"          => "show_featured_image_on_header",
                "title"         => "Featured Portfolio",
                "description"   => "Check if this portfolio is a featured one",
                "type"          => "checkbox",
                "scope"         =>   array( "portfolio" ),
                //"scope"         =>   array( "portfolio", "page" ),
                "capability"    => "manage_options"
            ),

			array(
                "name"          => "portfilio_head_style",
                "title"         => "Head style",
                "description"   => "Select Layout Style",
                "type"          => "dropdown",
				'options' => Array(
					'Show Featured Image'		=> 'featured_image',
					'Show Title'						=> 'title',
					'Blank'						=> 'blank',
				),
				"scope"         =>   array( "portfolio" ),
                "capability"    => "manage_options"
            ),

			array(
                "name"          => "layout_portfilio_detail_page",
                "title"         => "Layout Type",
                "description"   => "Select Layout Style",
                "type"          => "dropdown",
				'options' => Array(
					'Fullwidth Content After'		=> 'fullwidth-content-after',
					'Fullwidth Content Before'		=> 'fullwidth-content-before',
					'Right Sider'						=> 'right-sider',
				),
				"scope"         =>   array( "portfolio" ),
                "capability"    => "manage_options"
            ),
        );
        /**
        * PHP 4 Compatible Constructor
        */
        //function jvfrmPostCustomFields() { $this->__construct(); }
        /**
        * PHP 5 Constructor
        */
        function __construct() {
            add_action( 'admin_menu', array( &$this, 'createCustomFields' ) );
            add_action( 'save_post', array( &$this, 'saveCustomFields' ), 1, 2 );
            // Comment this line out if you want to keep default custom fields meta box
            add_action( 'do_meta_boxes', array( &$this, 'removeDefaultCustomFields' ), 10, 3 );
        }
        /**
        * Remove the default Custom Fields meta box
        */
        function removeDefaultCustomFields( $type, $context, $post ) {
            foreach ( array( 'normal', 'advanced', 'side' ) as $context ) {
                foreach ( $this->postTypes as $postType ) {
                    remove_meta_box( 'postcustom', $postType, $context );
                }
            }
        }
        /**
        * Create the new Custom Fields meta box
        */
        function createCustomFields() {
            if ( function_exists( 'add_meta_box' ) ) {
                foreach ( $this->postTypes as $postType ) {
                    add_meta_box( 'jvfrm-post-custom-fields', 'Additional information', array( &$this, 'displayCustomFields' ), $postType, 'normal', 'high' );
                }
            }
        }
        /**
        * Display the new Custom Fields meta box
        */
        function displayCustomFields() {
            global $post;
            ?>
            <div class="form-wrap">
                <?php
                wp_nonce_field( 'jvfrm-post-custom-fields', 'jvfrm-post-custom-fields_wpnonce', false, true );
                foreach ( $this->customFields as $customField ) {
                    // Check scope
                    $scope = $customField[ 'scope' ];
                    $output = false;
                    foreach ( $scope as $scopeItem ) {
                        switch ( $scopeItem ) {
                            default: {
                                if ( $post->post_type == $scopeItem )
                                    $output = true;									
                                break;
                            }
                        }
                        if ( $output ) break;
                    }
                    // Check capability
                    if ( !current_user_can( $customField['capability'], $post->ID ) )
                        $output = false;
                    // Output if allowed
                    if ( $output ) { ?>
                        <div class="form-field form-required">
                            <?php
                            switch ( $customField[ 'type' ] ) {
                                case "checkbox": {
                                    // Checkbox
                                    echo '<div class="jvfrm_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>';
									if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>';
									echo '</div>';
                                    echo '<div class="jvfrm_ctm_field"><input type="checkbox" name="' . $this->prefix . $customField['name'] . '" id="' . $this->prefix . $customField['name'] . '" value="yes"';
                                    if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == "yes" )
                                        echo ' checked="checked"';
                                    echo '" style="width: auto;" />';
									echo '</div>';
                                    break;									
                                }
								case "dropdown": {
                                    // Dropdown
                                    echo '<div class="jvfrm_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>';
									if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>';
									echo '</div>';
									echo '<div class="jvfrm_ctm_field">';
									echo '<select name="' . $this->prefix . $customField[ 'name' ] .'">';
									foreach( $customField['options'] as $label => $value )
									{
										printf( "<option value='{$value}' %s>{$label}</option>", selected( $value == get_post_meta( $post->ID, $this->prefix . $customField['name'], true), true, false ) );
									}
									echo '</select>';
									echo '</div>';                                   
                                    break;									
                                }
                                case "textarea":
                                case "wysiwyg": {
                                    // Text area
                                    echo '<div class="jvfrm_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label></div>';
                                    echo '<div class="jvfrm_ctm_field"><textarea name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" columns="30" rows="3">' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '</textarea>';
                                    // WYSIWYG
                                    if ( $customField[ 'type' ] == "wysiwyg" ) { ?>
                                        <script type="text/javascript">
                                            jQuery( document ).ready( function() {
                                                jQuery( "<?php echo $this->prefix . $customField[ 'name' ]; ?>" ).addClass( "mceEditor" );
                                                if ( typeof( tinyMCE ) == "object" && typeof( tinyMCE.execCommand ) == "function" ) {
                                                    tinyMCE.execCommand( "mceAddControl", false, "<?php echo $this->prefix . $customField[ 'name' ]; ?>" );
                                                }
                                            });
                                        </script>
                                    <?php }
									echo '</div>';
                                    break;									
                                }
                                default: {
                                    // Plain text field
                                    echo '<div class="jvfrm_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label></div>';
                                    echo '<div class="jvfrm_ctm_field"><input type="text" name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" value="' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '" />';
									echo '</div>';
                                    break;								
                                }
                            }
                            ?>
                            <?php if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>'; ?>
                        </div>
                    <?php
                    }
                } ?>
            </div>
            <?php
        }
        /**
        * Save the new Custom Fields values
        */
        function saveCustomFields( $post_id, $post ) {
            if ( !isset( $_POST[ 'jvfrm-post-custom-fields_wpnonce' ] ) || !wp_verify_nonce( $_POST[ 'jvfrm-post-custom-fields_wpnonce' ], 'jvfrm-post-custom-fields' ) )
                return;
            if ( !current_user_can( 'edit_post', $post_id ) )
                return;
            if ( ! in_array( $post->post_type, $this->postTypes ) )
                return;
            foreach ( $this->customFields as $customField ) {
                if ( current_user_can( $customField['capability'], $post_id ) ) {
                    if ( isset( $_POST[ $this->prefix . $customField['name'] ] ) && trim( $_POST[ $this->prefix . $customField['name'] ] ) ) {
                        $value = $_POST[ $this->prefix . $customField['name'] ];
                        // Auto-paragraphs for any WYSIWYG
                        if ( $customField['type'] == "wysiwyg" ) $value = wpautop( $value );
                        update_post_meta( $post_id, $this->prefix . $customField[ 'name' ], $value );
                    } else {
                        delete_post_meta( $post_id, $this->prefix . $customField[ 'name' ] );
                    }
                }
            }
        }
 
    } // End Class
 
} // End if class exists statement
 
// Instantiate the class
if ( class_exists('jvfrmPostCustomFields') ) {
    $jvfrmPostCustomFields_var = new jvfrmPostCustomFields();
}

