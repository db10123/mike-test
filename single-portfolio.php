<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Javo
 * @since Javo Themes 1.0
 */

if( ! defined( 'ABSPATH' ) )
	die( -1 );

$jvfrm_spot_author				= new WP_User( get_the_author_meta( 'ID' ) );

get_header();

function jvfrm_spot_single_post_content(){
	global $post;
	do_action( 'jvfrm_spot_post_content_before' );
	while ( have_posts() ) : the_post();
		get_template_part(
			apply_filters(
				'jvfrm_spot_single_template'
				, ( false !== get_post_format() ? 'content-' : 'content' ) . get_post_format()
				, get_the_ID()
			)
		);
	endwhile;
	do_action( 'jvfrm_spot_post_content_after' );

	// Only  Post type for "post"
	comments_template( '', true );
	jvfrm_spot_post_nav();
}

// Get page options
$portfolio_layout_style = jvfrm_spot_tso()->header->get("portfolio_detail_layout");
$portfolio_header_block_style = jvfrm_spot_tso()->header->get("portfolio_header_block_style");
echo "layout=". $portfolio_layout_style;
echo "layout=". $portfolio_header_block_style;


$portfolio_layout_style_single = get_post_meta( get_the_ID(), '_jvfrm_layout_portfilio_detail_page', true);
echo $portfolio_layout_style_single;

// Get Terms
function jvfrm_portpolio_cate($portfolio_tax='') {	
	$terms = get_the_terms( get_the_ID(), $portfolio_tax );                   
	if ( $terms && ! is_wp_error( $terms ) ) : 
     $portfolio_categories = array(); 
	    foreach ( $terms as $term ) {
		    $portfolio_categories[] = $term->name;
	    }
                         
		$portfolio_terms = join( ", ", $portfolio_categories );    
	 return $portfolio_terms;
	 endif;
}

function jvfrm_portfilio_meta(){
	$portfolio_meta = get_post_meta(get_the_ID());
	
	// To get all of meta value for debug 
	foreach($portfolio_meta as $key=>$val){
		//echo $key . ' : ' . $val[0] . '<br/>';
	}

	// Get each meta value for this portfolio 	
	$jvfrm_creation_date = get_post_meta( get_the_ID(), '_jvfrm_creation-date', true );
	$jvfrm_short_description = get_post_meta( get_the_ID(), '_jvfrm_short-description', true );
	$jvfrm_link_to = get_post_meta( get_the_ID(), '_jvfrm_link-to', true );
	if (false === strpos($jvfrm_link_to, '://')) {
		$jvfrm_link_url = 'http://' . $jvfrm_link_to;
	}


	$portfolio_layout_style_single='fullwidth-content-after';
		switch( get_post_meta( get_the_ID(), '_jvfrm_layout_portfilio_detail_page', true) ) :
			case 'fullwidth-content-after' :
				$portfolio_meta_content_col = "col-md-9";				
				$portfolio_meta_extra_info_col = "col-md-3";
			break;
			case 'fullwidth-content-before' :
				$portfolio_meta_content_col = "col-md-9";
				$portfolio_meta_extra_info_col = "col-md-3";
			break;

			case 'right-sider' :
			default :
				$portfolio_meta_content_col = "col-md-12";
			    $portfolio_meta_extra_info_col = "col-md-12";
				
				wp_reset_postdata();				
		endswitch;


		function jvfrm_portfilio_meta_content() {

		}

	echo '<div class="row portfolio-meta">';
	
	echo '<div class="' . $portfolio_meta_content_col .' portfolio-left">';	
	// Description
	echo '<h3>' .esc_html__( "About this project", 'javospot' ). '</h3>';
	if ( ! empty( $jvfrm_short_description ) ) echo '<p>' . $jvfrm_short_description. '</p>';
	echo '</div>';

	echo '<div class="'. $portfolio_meta_extra_info_col .'" portfolio-right">';
	
	// Date
	echo '<h4>' .esc_html__( "Date", 'javospot' ). '</h4>';
	if ( ! empty( $jvfrm_creation_date ) ) echo '<p>' . $jvfrm_creation_date. '</p>';
	
	// Link 
	echo '<h4>' .esc_html__( "Link", 'javospot' ). '</h4>';
	if ( ! empty( $jvfrm_link_to ) ) echo '<p><a href="'. $jvfrm_link_url .'" target="_blank">' . $jvfrm_link_to. '</a></p>';
	
	// Categories
	echo '<h4>' .esc_html__( "Category", 'javospot' ). '</h4>';
		 echo '<p>'. jvfrm_portpolio_cate('portfolio_category') .'</p>';

	// Tags
	echo '<h4>' .esc_html__( "Tag", 'javospot' ). '</h4>';
		 echo '<p>'. jvfrm_portpolio_cate('portfolio_tag') .'</p>';
	
	echo '</div>';
	echo '</div>';
}

//add_action( 'jvfrm_spot_post_content_inner_after', 'jvfrm_portfilio_meta', 10, 3);
add_action( 'jvfrm_spot_post_content_inner_right_sider', 'jvfrm_portfilio_meta', 10, 3);


function jvfrm_arrow_pagenation(){
	echo '<div class="row pagenation-inline-wrap">';
	echo '<div class="col-md-4 next-posts text-left">'. next_post_link('%link', '<span class="glyphicon glyphicon-chevron-left"></span>') .'</div>';
	echo '<div class="col-md-4 list-posts text-center"><a href="/portfolio/"><span class="glyphicon glyphicon-th-large"></span></a></div>';
	echo '<div class="col-md-4 prev-posts text-right">'. previous_post_link('%link', '<span class="glyphicon glyphicon-chevron-right"></span>') .'</div>';
	echo '</div>';
}

add_action( 'jvfrm_spot_post_content_inner_after', 'jvfrm_arrow_pagenation', 10, 3);




do_action( 'jvfrm_spot_single_page_before' ); ?>
<?php
if( jvfrm_spot_single_post_type() == 1){ ?>
	<div class="jv-single-post-layout-1">
		<a href="<?php the_permalink(); ?>" class="jv-single-post-thumbnail-holder">
			<?php the_post_thumbnail( 'full', Array( 'class' => 'img-responsive' ) ); ?>
		</a>
		<div class="filter-overlay"></div>
		<div class="jv-single-post-title-container">
			<div class="jv-single-post-title-wrap">
				<div class="jv-single-post-title-category admin-color-setting"><?php	echo jvfrm_portpolio_cate('portfolio_category'); ?></div>
				<h1 class="jv-single-post-title"><?php the_title(); ?></h1>

				<div class="jv-single-post-scroll-trigger-wrap javo-spyscroll-icon">
					<a href="#post-<?php the_ID();?>" class="jv-single-post-scroll-trigger"><i class="fa fa-angle-down"></i></a>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div id="jv-single-fixed-navigations">
	<?php
	jvfrm_spot_post_nav(
		Array(
			'post_thumbnail'	=> true,
			'type'				=> 'reavl',
		)
	); ?>
</div>

<div class="container">
	<div class="row">
		<?php
		$portfolio_layout_style_single='fullwidth-content-after';
		switch( get_post_meta( get_the_ID(), '_jvfrm_layout_portfilio_detail_page', true ) ) :
			case 'fullwidth-content-after' :
				echo '<div class="col-md-12 main-content-wrap">';
					jvfrm_spot_single_post_content();
				echo '</div>';
			break;
	
			case 'right-sider' :
			default :
				echo '<div class="col-md-9 main-content-wrap">';
					jvfrm_spot_single_post_content();
				echo '</div>';
				wp_reset_postdata();
				echo '<div class="col-md-3"><div class="theiaStickySidebar">';
					do_action( 'jvfrm_spot_post_content_inner_right_sider' );
				echo '</div></div>';
				//get_sidebar();
		endswitch; ?>
	</div>	
</div> <!-- container -->

<?php
do_action( 'jvfrm_spot_single_page_after' );
get_footer();