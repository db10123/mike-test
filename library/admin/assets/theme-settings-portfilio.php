<?php
$jvfrm_spot_options = Array(
	'header_type' => apply_filters( 'jvfrm_spot_options_header_types', Array() )
	, 'header_skin' => Array(
		esc_html__("Dark", 'javospot')							=> ""
		, esc_html__("Light", 'javospot')						=> "light"
	)
	, 'able_disable' => Array(
		esc_html__("Disable", 'javospot')					=> "disabled"
		,esc_html__("Able", 'javospot')							=> "enable"

	)
	, 'header_fullwidth' => Array(
		esc_html__("Center", 'javospot')						=> "fixed"
		, esc_html__("Wide", 'javospot')						=> "full"
	)
	, 'header_relation' => Array(
		esc_html__("Transparency menu", 'javospot')	=> "absolute"
		,esc_html__("Default menu", 'javospot')				=> "relative"
	)
	, 'header_block_style' => Array(
		esc_html__("Show Title", 'javospot')	=> "title_only"
		,esc_html__("Show featured image with Title", 'javospot')	=> "featured_image"
		,esc_html__("Show Fixed image with Title ", 'javospot')				=> "relative"
	)

	, 'portfolio_detail_layout' => Array(
		esc_html__("Type A", 'javospot')	=> "type_a"
		,esc_html__("Type B", 'javospot')				=> "type_b"
	)
); ?>

<div class="jvfrm_spot_ts_tab javo-opts-group-tab hidden" tar="portfilio">
	<h2><?php esc_html_e("Portfilio Page Settings", 'javospot'); ?> </h2>
	<table class="form-table">
	
	<tr><th>
		<?php esc_html_e( "Header / Menu", 'javospot');?>
		<span class="description"></span>
	</th><td>		

		<h4><?php esc_html_e( "Detail Page Menu", 'javospot'); ?></h4><hr>
		<fieldset class="inner">
			<dl>
				<dt><?php esc_html_e( "Navi Type", 'javospot');?></dt>
				<dd>
					<select name="jvfrm_spot_ts[hd][portfolio_header_relation]">
						<?php
						foreach( $jvfrm_spot_options['header_relation'] as $label => $value )
						{
							printf( "<option value='{$value}' %s>{$label}</option>", selected( $value == jvfrm_spot_tso()->header->get("portfolio_header_relation"), true, false ) );
						} ?>
					</select>
					<div class="description"><?php esc_html_e("Caution: If you choose transparent menu type, page's main text contents ascends as much as menu's height to make menu transparent.", 'javospot');?></div>
				</dd>
			</dl>

			<dl>
				<dt><?php esc_html_e( "Background Color", 'javospot');?></dt>
				<dd>
					<input name="jvfrm_spot_ts[portfolio_page_menu_bg_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get( 'portfolio_page_menu_bg_color' ) );?>" class="wp_color_picker" data-default-color="#000000">
				</dd>
			</dl>

			<dl>
				<dt><?php esc_html_e( "Text Color", 'javospot');?></dt>
				<dd>
					<input name="jvfrm_spot_ts[portfolio_page_menu_text_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get( 'portfolio_page_menu_text_color' ) );?>" class="wp_color_picker" data-default-color="#ffffff">
				</dd>
			</dl>

			<dl>
				<dt><?php esc_html_e( "Initial Header Transparency", 'javospot');?></dt>
				<dd>
					<input type="text" name="jvfrm_spot_ts[hd][portfolio_header_opacity]" value="<?php echo esc_attr( jvfrm_spot_tso()->header->get("portfolio_header_opacity", 0 ) );?>">
					<div class="description"><?php esc_html_e("Please enter numerical value from 0.0 to 1.0. Higer value put in, more transparent it will be. <br> Ex) 0.5=opaque", 'javospot');?></div>
				</dd>
			</dl>
		</fieldset>

		<h4><?php esc_html_e( "Header Style", 'javospot'); ?></h4>
		<fieldset class="inner">
			<dl>
				<dt><?php esc_html_e("Header type", 'javospot'); ?></dt>
				<dd>
					<select name="jvfrm_spot_ts[hd][portfolio_header_block_style]">
						<?php
						foreach( $jvfrm_spot_options['header_block_style'] as $label => $value )
						{
							printf( "<option value='{$value}' %s>{$label}</option>", selected( $value == jvfrm_spot_tso()->header->get("portfolio_header_block_style"), true, false ) );
						} ?>
					</select>
					<div class="description"><?php esc_html_e("Caution: If you choose transparent menu type, page's main text contents ascends as much as menu's height to make menu transparent.", 'javospot');?></div>					
				</dd>
			</dl>
		</fieldset>
	</td></tr>


	<tr>
		<th>
			<?php esc_html_e( "Default Style", 'javospot');?>
			<span class="description"></span>
		</th>
		<td>		
		<h4><?php esc_html_e( "Setup Portfilio List Page", 'javospot'); ?></h4>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[portfilio_list_page]">
				<?php
				if( $pages = get_posts( "post_type=page&post_status=publish&posts_per_page=-1&suppress_filters=0" ) )
				{
					printf( "<optgroup label=\"%s\">", esc_html__( "Select a page for portfolio list", 'javospot' ) );
					foreach( $pages as $post )
						printf(
							"<option value=\"{$post->ID}\" %s>{$post->post_title}</option>"
							, selected( $post->ID == $jvfrm_spot_tso->get( 'portfilio_list_page', '' ), true, false )
						);
					echo "</optgroup>";
				} ?>
			</select>
		</fieldset>

		<h4><?php esc_html_e( "Default Page Layout", 'javospot'); ?></h4>
<?php 
		$layout = jvfrm_spot_tso()->header->get("portfolio_header_block_style");
echo "layout=". $layout;
?>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[hd][portfolio_detail_layout]">
						<?php
						foreach( $jvfrm_spot_options['portfolio_detail_layout'] as $label => $value )
						{
							printf( "<option value='{$value}' %s>{$label}</option>", selected( $value == jvfrm_spot_tso()->header->get("portfolio_detail_layout"), true, false ) );
						} ?>
					</select>
					<div class="description"><?php esc_html_e("Caution: If you choose transparent menu type, page's main text contents ascends as much as menu's height to make menu transparent.", 'javospot');?></div>		
		</fieldset>
		</td>
	</tr>
	</table>
</div>